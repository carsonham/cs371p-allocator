
#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <vector>

#include "Allocator.h"

using namespace std;

void allocator_solve (istream& r, ostream& w) {
    int tests;
    r >> tests;
    {
        string garbage;
        getline(r, garbage);
        getline(r, garbage);
    }
    string line;
    while (tests-- > 0) {
        my_allocator<double, 1000> a;
        getline(r, line);
        while (line != "") {
            int request = stoi(line);
            if (request >= 0) {
                a.allocate(request);
            } else {
                auto it = begin(a);
                int freeBlocks = 0;
                while (freeBlocks != abs(request) - 1) {
                    if (*it < 0) {
                        ++freeBlocks;
                    }
                    ++it;
                }
                while (*it > 0) ++it;
                a.deallocate((double*)((((int*)(&(*it)))) + 1), 0);
            }
            getline(r, line);
        }
        auto b = begin(a);
        auto e = end(a);
        vector<int> v;
        while (b != e) {
            v.push_back(*b);
            ++b;
        }
        for (int i = 0; i < (int)v.size() - 1; ++i) {
            w << v[i] << " ";
        }
        w << v[v.size() - 1];
        w << endl;
    }
}

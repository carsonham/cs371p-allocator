// --------------
// RunAllocator.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.h"

// ----
// main
// ----

int main () {
    using namespace std;
    allocator_solve(cin, cout);
    return 0;
}

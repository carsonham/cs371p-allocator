// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    /**
     * Returns true if a given address falls within
     * the heap memory, false otherwise
     */
    bool inBounds(int* p) const {
        return (p >= (int*) a) && (p <= (int*)((char*)a) + N);
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& rhs, const iterator& lhs) {
            return rhs._p == lhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) :
            _p(p) {}

        // ----------
        // operator *
        // ----------

        int& operator * () {
            return *_p;
        }

        /**
         * If this iterator is pointing to an begin sentinel,
         * it will return the corresponding end sentinel
         */
        int& getEndSentinel() {
            int* endSentinel = (int*)(((char*)_p) + abs(*_p) + 4);
            return *endSentinel;
        }

        /**
         * If this iterator is pointing to an end sentinel,
         * it will return the corresponding begin sentinel
         */
        int& getBeginSentinel() {
            int* beginSentinel = (int*)(((char*)_p) - abs(*_p) - 4);
            return *beginSentinel;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            char* temp = (char*) _p;
            temp += abs(*_p) + 8;
            _p = (int*) temp;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) throw bad_alloc();
        (*this)[0] = N - 8;
        (*this)[N - 4] = N - 8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * Allocates memory for n objects of type T, each the size of T
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param n : number of T objects to allocate space for
     * @return pointer to beginning of data
     */
    pointer allocate (size_type n) {
        bool allocated = false;
        iterator b = begin();
        iterator e = end();
        pointer p = 0;
        int allocSize = (int)(2 * sizeof(int) + n * sizeof(T));

        while (b != e && !allocated) {
            int need = n * sizeof(T);
            if (*b >= need) {                                               // found a block big enough
                int& beginSentinel = *b;
                int& endSentinel = b.getEndSentinel();
                if ((*b - need) < (int)(2 * sizeof(int) + sizeof(T))) {     // allocate entire block
                    beginSentinel = -beginSentinel;
                    endSentinel = -endSentinel;
                } else {                                                    // split the block
                    int newSize = -(allocSize - 8);
                    beginSentinel = newSize;
                    int& temp = iterator(&beginSentinel).getEndSentinel();
                    temp = newSize;
                    int& remainderSentinel = *(((int*)&temp) + 1);
                    remainderSentinel = endSentinel + newSize - 8;
                    int& temp2 = iterator(&remainderSentinel).getEndSentinel();
                    temp2 = endSentinel + newSize - 8;
                }
                p = (pointer) (((int*)&beginSentinel) + 1);
                allocated = true;
            }
            ++b;
        }
        assert(valid());
        if (p == 0) throw bad_alloc();
        return p;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());                            // from the prohibition of new
    }



    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * Frees up a block in the heap at the given pointer.
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param p pointer to block of data that needs to be deallocated
     */
    void deallocate (pointer p, size_type) {
        assert(inBounds((int*)p));
        int& beginSentinel = *((int*)p - 1);
        int& endSentinel = iterator(&beginSentinel).getEndSentinel();
        beginSentinel = abs(beginSentinel);
        endSentinel = abs(endSentinel);
        int size = beginSentinel;
        int* l = (((int*)&beginSentinel) - 1);
        int* r = (((int*)&endSentinel) + 1);
        int* beginPtr = &beginSentinel;

        if (inBounds(l) && *l > 0) {               // merge left block
            int& leftSentinel = *l;
            size += leftSentinel + 8;
            int& newBeginSentinel = iterator(&leftSentinel).getBeginSentinel();
            newBeginSentinel = size;
            endSentinel = size;
            beginPtr = &newBeginSentinel;
        }

        if (inBounds(r) && *r > 0) {                // merge right block
            int& rightSentinel = *r;
            size += rightSentinel + 8;
            int& endSentinel = iterator(&rightSentinel).getEndSentinel();
            endSentinel = size;
            *beginPtr = size;
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }


    /**
     * Returns an instance of iterator
     * at the beginning of an allocator
     */
    const iterator begin() const {
        int* p = (int*) a;
        iterator it(p);
        return it;
    }

    /**
     * Returns an instance of iterator
     * at the end of an allocator
     */
    const iterator end() const {
        int* p = (int*)(((char*)a) + N);
        return iterator(p);
    }

    /**
     * Returns an instance of iterator
     * at the beginning of an allocator
     */
    iterator begin() {
        return iterator((int*)a);
    }

    /**
     * Returns an instance of iterator
     * at the end of an allocator
     */
    iterator end() {
        return iterator((int*)(((char*)a) + N));
    }

public:
    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Checks to make sure beginning and end sentinel values match,
     * that the heap is the correct total size, that no blocks are
     * of an invalid size, and that there are no two free blocks
     * contiguous in memory
     * @return true if heap has valid structure, false otherwise
     */
    bool valid () const {
        long unsigned int totalBytes = 0;
        bool prevFree = false;
        int beginSentinel;
        iterator b = begin();
        iterator e = end();

        while (b != e) {
            beginSentinel = *b;
            if (beginSentinel == 0) return false;
            if (beginSentinel > 0) {
                if (prevFree) {
                    return false;
                }
                prevFree = true;
            } else prevFree = false;
            int& endSentinel = b.getEndSentinel();
            if (beginSentinel != endSentinel) return false;
            totalBytes += (beginSentinel < 0 ? beginSentinel * -1 : beginSentinel) + 8;
            ++b;
        }
        if (totalBytes != N) return false;

        return true;
    }
};

/**
 * Solve the allocator problem
 */
void allocator_solve (istream& r, ostream& w);

#endif // Allocator_h
